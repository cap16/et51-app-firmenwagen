sap.ui.define([
  "de/bit/et/firmenwagen/controller/BaseController",
  "sap/ui/model/json/JSONModel",
  "../model/formatter",
  "sap/ui/core/Core",
  "sap/ui/core/message/Message",
  "sap/m/MessagePopover",
  "sap/ui/core/MessageType",
  "sap/m/MessageItem",
  "sap/m/MessageBox",
  "sap/ui/core/syncStyleClass"
], function(Controller, JSONModel, formatter, Core, Message, MessagePopover, MessageType, MessageItem, MessageBox, syncStyleClass) {
  "use strict";

  return Controller.extend("de.bit.et.firmenwagen.controller.Home", {

		formatter: formatter,

		onInit : function () {
			this._oLogger = this.getLogger("de.bit.et.firmenwagen.controller.Home");

			var oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				level: "Senior Consultant",
				user: "Tobias Hofmann",
				manager: "Martin Fischer",
				date: new Date(),
				wizardComplete: false,
				showChargingStationOption: false,
				showFooter: false,
				showSave: false,
				showCancel: false
			});
			this.setModel(oViewModel, "homeView");

			var oClaimModel = new JSONModel({
				leasingCompany: "",
				category: "",
				categoryId: "",
				fuelType: "",
				fuelTypeId: "",
				chargingStation: false,
				leasing: 0,
				service: 0,
				efficiency: "",
				efficiencyId: "",
				consumption: 0,
				price: 0,
				additionalCostsLeasingService: 223,
				savingsConsumption: 175,
				additionalCostsSum: 47,
				date: ""		
			});
			this.setModel(oClaimModel, "claim");

			var oAttachmentModel = new JSONModel([]);
			this.setModel(oAttachmentModel, "file");

			// Message Manager
			this._MessageManager = Core.getMessageManager();
			this.getView().setModel(this._MessageManager.getMessageModel(), "message");

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());

			this.getRouter().getRoute("Home").attachPatternMatched(this._onObjectMatched, this);

		},

		onMessagesButtonPress: function (oEvent) {
			this.createMessagePopover();
			this.oMP.toggle(oEvent.getSource());
		},
		
		createMessagePopover: function () {
			this.oMP = new MessagePopover({
				activeTitlePress: function (oEvent) {
					var oItem = oEvent.getParameter("item");
					var oMessage = oItem.getBindingContext("message").getObject();
					var oControl = sap.ui.core.Element.registry.get(oMessage.getControlId());
					if (oControl) {
						oControl.focus();
					}
				}.bind(this),
				
				items: {
					path:"message>/",
					template: new MessageItem(
						{
							title: "{message>message}",
							subtitle: "{message>additionalText}",
							activeTitle: {parts: [{path: 'message>controlIds'}], formatter: this.isPositionable},
							type: "{message>type}",
							description: "{message>message}"
						})
				},
				groupItems: true
			});

			var oBtn = this.getView().byId("messagePopoverBtn");			
			oBtn.addDependent(this.oMP);
		},

		isPositionable : function (sControlId) {
			return sControlId ? true : true;
		},

		onFilterSelect: function(oEvent) {
			if (oEvent.getParameter("selectedKey") === "wizard") {
				this.getModel("homeView").setProperty("/showFooter", true);
				this.getModel("homeView").setProperty("/showCancel", true);
			} else if (oEvent.getParameter("selectedKey") === "validate"){
				this.getModel("homeView").setProperty("/showFooter", true);
				this.getModel("homeView").setProperty("/showCancel", true);
				this.getModel("homeView").setProperty("/showSave", true);
				console.log("adf ");
			} else {
				this.getModel("homeView").setProperty("/showFooter", false);
				this.getModel("homeView").setProperty("/showCancel", false);
				this.getModel("homeView").setProperty("/showSave", false);
			}

		},

		onSelectCategory: function(oEvent) {
			console.log("onSelectCategory");
			var oObject = oEvent.getSource().getBindingContext().getObject();
			this.getModel("claim").setProperty("/category", oObject.name);
			this.getModel("claim").setProperty("/categoryId", oObject.ID);
			var sPath = this.getView().getBindingContext().getPath();
			var oModel = this.getView().getBindingContext().getModel();
			oModel.setProperty("/category_ID", oObject.ID);
			
			var oWizard = this._getWizard();
			var oStep = this._getWizardStep("SelectCategory");

			oWizard.validateStep(oStep);
		},


		onEfficiencySelected: function(oEvent) {
			console.log("onEfficiencySelected");
			var sNewValue = oEvent.getParameter("newValue");
			
			var sObjectPath = this.getModel().createKey("Efficiency", {
				ID :  sNewValue
			});
			var oEfficiency = this.getModel().getProperty("/" + sObjectPath);

			this.getModel("claim").setProperty("/efficiencyId", sNewValue);
			this.getModel("claim").setProperty("/efficiency", oEfficiency.name);
		},


		onFuelTypeSelected: function(oEvent) {
			console.log("onFuelTypeSelected");
			console.log(oEvent.getParameters());
			console.log(oEvent.getParameter("selectedItem"));

			// set selected fuel type to model
			this.getModel("claim").setProperty("/fuelType", oEvent.getParameter("selectedItem").getProperty("text"));
			this.getModel("claim").setProperty("/fuelTypeId", oEvent.getParameter("selectedItem").getProperty("key"));

			// this will show the switch option to the user
			var bChargingStation = oEvent.getParameter("selectedItem").data("chargingStation");
			console.log("Charging station: " + bChargingStation);
			this.getModel("homeView").setProperty("/showChargingStationOption", bChargingStation);

			var oWizard = this._getWizard();
			var oStep = this._getWizardStep("SelectType");

			oWizard.validateStep(oStep);
		},

		wizardCompletedHandler: function(oEvent){
			console.log("wizardCompletedHandler");

			// finish wizard
			this.getModel("homeView").setProperty("/wizardComplete", true);

			// move to tab validate
			this.byId("idIconTabBar").setSelectedKey("validate");

			// set footer buttons visibility
			this.getModel("homeView").setProperty("/showFooter", true);
			this.getModel("homeView").setProperty("/showCancel", true);
			this.getModel("homeView").setProperty("/showSave", true);

			var oClaimModel = this.getModel("claim");			
			// get leasing value
			var iLeasing = this._getWizardInput("idleasing").getValue();
			oClaimModel.setProperty("/leasing", iLeasing);
			// get service maintenance value
			var iService = this._getWizardInput("idmaintenance").getValue();
			oClaimModel.setProperty("/service", iService);
			// get consumption value
			var iConsumption = this._getWizardInput("idconsumption").getValue();
			oClaimModel.setProperty("/consumption", iConsumption);
			// get price value
			var iPrice = this._getWizardInput("idPrice").getValue();
			oClaimModel.setProperty("/price", iPrice);


			this._MessageManager.removeAllMessages();

			var oValidateLeasing = this._getInput("inpValidateLeasing");
			var oValidateService = this._getInput("inpValidateService");
			var oValidateConsumption = this._getInput("inpValidateConsumption");
			var oValidatePrice = this._getInput("inpValidatePrice");

			if (oValidateLeasing.getValue() <= 0) {
				this._createErrorMessage("Eingabe Leasing fehlerhaft", oValidateLeasing);
			}
			if (oValidateService.getValue() <= 0) {
				this._createErrorMessage("Eingabe Service fehlerhaft", oValidateService);
			}
			if (oValidateConsumption.getValue() <= 0) {
				this._createErrorMessage("Eingabe Verbrauch fehlerhaft", oValidateConsumption);
			}
			if (oValidatePrice.getValue() <= 0) {
				this._createErrorMessage("Eingabe Preis fehlerhaft", oValidatePrice);
			}
		},

		doSave: function() {
			console.log("doSave");

			this._getAccessToken();



			var oModel = this.getModel("claim");
			var oData = oModel.getData();
			var sPath = this.getView().getBindingContext().getPath();

			var oClaimData = new Object();
			oClaimData.name = "Tobias"
			oClaimData.leasingCompany = "VW";
			oClaimData.leasing = oData.leasing;			
			oClaimData.maintenance = oData.service;			
			oClaimData.efficiency = { ID: oData.efficiencyId};
			oClaimData.category = {ID: oData.categoryId};
			oClaimData.fueltype = {ID: oData.fuelTypeId};
			//oClaimData.category = oData.categoryId;			
			//oClaimData.fueltype = oData.fuelType;			
			oClaimData.consumption = oData.consumption;					
			oClaimData.price = oData.price;			
			oClaimData.chargingStation = oData.chargingStation;
			//oClaimData.additionalCostsLeasingService = oData.additionalCostsLeasingService;			
			//oClaimData.additionalCostsSum = oData.additionalCostsSum;
			//oClaimData.savingsConsumption = oData.savingsConsumption;

			oClaimData.additionalCostsSum = 47;
			oClaimData.additionalCostsLeasingService = 223;			
			oClaimData.savingsConsumption = 175;

			oClaimData.user = {ID: "34eadc90-f9dc-4f51-843e-a06cd3dfcf69"};
			oClaimData.manager = {ID: "55eadc90-f9dc-4f51-843e-a06cd3dfcf69"};

			//oClaimData.leasing = 1.0;
			//oClaimData.maintenance = 1.0;
			//oClaimData.consumption = 2.0;	
			//oClaimData.price = 20456;
			oClaimData.additionalCostsLeasingService = 12.2;
			oClaimData.additionalCostsSum = 13.3;
			oClaimData.savingsConsumption = 11.0;
			
			// set status of new claim to created
			oClaimData.status = 1;
			
			// create
			this.getModel().create(
				"/Claim",
				oClaimData,
				{
					success: function() {
						MessageBox.success("Antrag erfolgreich eingereicht");
					},
					error: function() {
						MessageBox.error("Nein, du nicht!");
					}
				}
			)
		},


		onShowTechnicalData: function(oEvent) {
			if (!this._oInfoDialog) {
				var oView = this.getView();
				this._oInfoDialog = sap.ui.core.Fragment.load({
					id: oView.getId(),
					name: "de.bit.et.firmenwagen.view.fragments.InfoDialog",
					controller: this
				}).then(function (oDialog){
					oView.addDependent(oDialog);
					syncStyleClass(oView.getController().getOwnerComponent().getContentDensityClass(), oView, oDialog);
					return oDialog;
				}.bind(this));
			}
			this._oInfoDialog.then(function(oDialog){
				oDialog.open();
			}.bind(this));
		},


		closeDialog: function() {
			this._oInfoDialog.then(function(oDialog){
				oDialog.close();
			}.bind(this));
		},


		/**
		 * get a new valid access token
		 * use refresh token to renew access token
		 */
		_getAccessToken: function() {
			console.log("_getAccessToken");

			// parameters
			var baseUrl = "/oauth/token";
			var refresh_token = "4db32701a92043a7b2cc8a84282e897e-r";
			var grant_type = "refresh_token";
			var username = "sb-clone-38f5b8a9-4892-4726-8db8-2283f9102565!b65780|workflow!b10150";
			var client_credentials = "b77c97ff-40a7-438e-9849-66e19522b636$aTP8r42PRLKg0h6q17GQBGfrLe5VzXCwOVPyaRckuTM=";

			// prepare data
			var credentials = username + ":" + client_credentials;
			var base64Credentials = btoa(credentials);
			var url = baseUrl + "?refresh_token=" + refresh_token + "&grant_type="+grant_type;
			
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", "Basic " + base64Credentials);
				},
	            context: this,
	            type: "GET",
	            success: this._getWorkflowStep
	        });
		},

		_getWorkflowStep: function(oJwt) {
			console.log("_getWorkflowStep " + oJwt);

			var auth = "Bearer " + oJwt.access_token;
			//var url = "https://api.workflow-sap.cfapps.eu10.hana.ondemand.com/workflow-service/rest/v1/workflow-instances";
			var url = "/workflow-service/rest/v1/workflow-instances";
			console.log(url);
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", auth);
				},
	            context: this,
	            type: "GET",
				success: function(response) {
					console.log(response[0]);
					console.log(response[0].id);
					//console.log(auth);
					var sWorkflowInstanceId = response[0].id;
					this._sendIntermediateMessage(auth, sWorkflowInstanceId);
				},
				error: function(err) {
					console.log("error");
					console.log(err);
				}
	        });

			/*
			success: this._sendIntermediateMessage,	        
			success: function(response) {
				console.log(response);
				console.log(response.id);
				console.log(auth);
				var sWorkflowInstanceId = response.id;
				this._sendIntermediateMessage(auth, sWorkflowInstanceId);
			}.bind(this),
			*/
		},


		/**
		 * Confirm that step was successfulle executed
		 */
		_sendIntermediateMessage: function(sAuth, sWorkflowInstanceId) {
			//console.log("_sendIntermediateMessage: " + sAuth + " " + sWorkflowInstanceId);

			var oBody = {};
			oBody.definitionId = "formcomplete";
			oBody.workflowInstanceId = sWorkflowInstanceId;

			var url = "/workflow-service/rest/v1/messages";
			var contentType = "application/json";
			
			console.log(url);
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", sAuth);
					xhr.setRequestHeader("Content-Type", contentType);
				},
				context: this,
				dataType: "json",
	            data: JSON.stringify(oBody),
	            type: "POST",
	            success: function(response) {
					console.log(response);
					console.log(response[0].id);
					
				},
				error: function(error) {
					console.log("_sendIntermediateMessage error");
					console.log(error);
				}
	        })
		},

		_createErrorMessage: function(sMessage, oInput) {
			//var sPath = oInput.getBindingContext().getPath();
			var sPath = "";
			var sProperty = oInput.getBindingPath("value");
			//var sTarget = sPath + "/" + sProperty;
			var sTarget = sPath + sProperty;
			var oMessage = new Message({
				message: sMessage,
				type: MessageType.Error,
				target: sTarget,
				processor: this.getView().getModel("claim")
			});
			Core.getMessageManager().addMessages(oMessage);
		},
		
		/**
		 * Create a new OData v4 entity
		 * @private
		 */
		_createEmptyEntity: function() {

			console.log("_createEmptyEntity");
			var oContext = this.getModel().createEntry("/Claim", {
				success: function(oResult) {
					console.log("success");
					console.log(oResult);
				},
				error: function(oError) {
					console.log("error");
					console.log(oError);
				}
			});
			console.log(oContext);
			console.log(oContext.sPath);
			this.getView().bindElement(oContext.sPath);
			/*
			// form	
			var fragmentWizardId = this.getView().createId("fragmentWizard");
			var oForm = sap.ui.core.Fragment.byId(fragmentWizardId, "smartForm");
			console.log(oForm);

			var oView = this.getView();

			var oList = this.byId("claimList");
			var oBinding = oList.getBinding("items"),
			// Create a new entry through the table's list binding
			oContext = oBinding.create({				
				"name" : "test",
				"price": "12000.00"
			});
			//console.log(oContext);
			console.log(oContext.sPath);
			oContext.created().then(
				function () {
					console.log("success");
					console.log(oContext.sPath);
					
					this.getView().bindElement(oContext.sPath);
					//oForm.bindElement({path : oContext.sPath});
				}.bind(this),
				function (oError) {
					console.log("error");
				}
			);

			var sGroupId = "claimGroup";
			oView.getModel().submitBatch(sGroupId);
			*/

			/*	
			}.bind(this)).catch(function (oError) {
				console.log("erro oBinding creater");
				console.log(oError);
			});
			*/
			

			
			//oForm.bindElement({path : oContext.sPath});


	
			
			/*
			var oItemTemplate = new sap.m.ColumnListItem();
			var oBindList = new sap.m.List({
						items: {
							path: "/Leasing",
							template: oItemTemplate
						}
					});
			this.getView().addDependent(oBindList);
			var oBinding = oBindList.getBinding("items");			
			var oContext = oBinding.create({
				name: "test",
				descr: "lorem ipsum"
				
				}
			);

			/*
			leasing: new sap.ui.model.odata.type.Decimal(),
				maintenance: new sap.ui.model.odata.type.Decimal(),
				consumption: new sap.ui.model.odata.type.Decimal(),
				price: new sap.ui.model.odata.type.Decimal(),
				efficiency: "",
				category: "",
				fueltype: "",				
				currency: ""
			*/
			/*
			console.log(oContext);
			oContext.created().then(function () {
				console.log("success");
			}, function (oError) {
				console.log("erro oBinding creater");
				console.log(oError);
			}).catch(function (oError) {
				console.log("erro oBinding creater");
				console.log(oError);
			}).finally(function (oError) {
				console.log("erro oBinding creater");
				console.log(oError);
			});
			*/
		},

		_getWizard: function() {
			var sFragmentId = this.getView().createId("fragmentWizard");
			var oWizard = sap.ui.core.Fragment.byId(sFragmentId, "CreateProductWizard");
			return oWizard;
		},

		_getWizardStep: function(sStep) {
			var sFragmentId = this.getView().createId("fragmentWizard");
			var oStep = sap.ui.core.Fragment.byId(sFragmentId, sStep);
			return oStep;
		},

		_getInput: function(sInput) {
			var sFragmentId = this.getView().createId("fragmentResult");
			var oInput = sap.ui.core.Fragment.byId(sFragmentId, sInput);
			return oInput;
		},

		_getWizardInput: function(sInput) {
			var sFragmentId = this.getView().createId("fragmentWizard");
			var oInput = sap.ui.core.Fragment.byId(sFragmentId, sInput);
			return oInput;
		},

		_onObjectMatched : function (oEvent) {
			this._oLogger.trace("_onObjectMatched");

			this._createEmptyEntity();

			this._MessageManager.removeAllMessages();
			this._MessageManager.registerObject(this.getView(), true);

			/*
			var oModel = this.getModel("infogantt");
			console.log(oModel);

			var startTimeUser = new Date();
			var endTimeUser = new Date(Date.now() + 1000 * 60 * 60 * 24 * 7);
			var startTimeManager = endTimeUser;
			var endTimeManager =  new Date(new Date(endTimeUser).getTime() + 1000 * 60 * 60 * 24 * 10);
			var startTimeLeasinggeber = endTimeManager;
			var endTimeLeasinggeber = new Date(new Date(endTimeManager).getTime() + 1000 * 60 * 60 * 24 * 20);

			console.log("startTimeUser: " + startTimeUser.getTime());
			console.log("endTimeUser: " + endTimeUser.getTime());
			console.log("startTimeManager: " + startTimeManager.getTime());
			console.log("endTimeManager: " + endTimeManager.getTime());
			console.log("startTimeLeasinggeber: " + startTimeLeasinggeber.getTime());
			console.log("endTimeLeasinggeber: " + endTimeLeasinggeber.getTime());

			var processStartTime = startTimeUser.getFullYear() + "0" + (startTimeUser.getMonth()+1) + "" + startTimeUser.getDate() + "000000";
			var processEndTime =  endTimeLeasinggeber.getFullYear() + "0" + (endTimeLeasinggeber.getMonth()+1) + "" + endTimeLeasinggeber.getDate() + "000000";

			oModel.setProperty("/root/startTimeProcess", processStartTime);
			oModel.setProperty("/root/endTimeProcess", processEndTime);		

			oModel.setProperty("/root/children/0/startTime", startTimeUser.getTime());
			oModel.setProperty("/root/children/0/endTime", endTimeUser.getTime());
			oModel.setProperty("/root/children/1/startTime", startTimeManager.getTime());
			oModel.setProperty("/root/children/1/endTime", endTimeManager.getTime());
			oModel.setProperty("/root/children/2/startTime", startTimeLeasinggeber.getTime());
			oModel.setProperty("/root/children/2/endTime", endTimeLeasinggeber.getTime());
			oModel.setProperty("/root/children/3/startTime", endTimeLeasinggeber.getTime());
			oModel.setProperty("/root/children/3/endTime", endTimeLeasinggeber.getTime());
			console.log(oModel.getData());
			*/
			/*
			// calendar special days			
			var fragmentResultId = this.getView().createId("fragmentResult");
			var oCal1 = this.byId("calendar1");
			var oLeg1 = this.byId("legend1"),
			*/
		}
	});
});
