sap.ui.define([
	"./BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("de.bit.et.firmenwagen.controller.NotFound", {

		/**
		 * Navigates to home when the link is pressed
		 * @public
		 */
		onLinkPressed : function () {
			this.getRouter().navTo("home");
		}

	});

});