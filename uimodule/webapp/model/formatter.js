sap.ui.define([], function () {
	"use strict";
	return {

		co2color: function(iValue) {
			if (iValue < 120) {
				return "Good";
			} else if (iValue >= 120 && iValue < 150) {
				return "Critical";
			} else {
				return "Error";
			}
		},

		fnTimeConverter: function (sTimestamp) {
			return sap.gantt.misc.Format.abapTimestampToDate(sTimestamp);
		},

		getTime: function(sTimestamp) {
			return new Date(sTimestamp);
		},

		endTimeProcess: function() {
			return new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 10)
		},
	};
});
