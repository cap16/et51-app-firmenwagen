sap.ui.define(["sap/ui/test/Opa5"], function (Opa5) {
    "use strict";

    return Opa5.extend("de.bit.et51.ui5.firmenwagen.test.integration.arrangements.Startup", {
        iStartMyApp: function () {
            this.iStartMyUIComponent({
                componentConfig: {
                    name: "de.bit.et51.ui5.firmenwagen",
                    async: true,
                    manifest: true
                }
            });
        }
    });
});
